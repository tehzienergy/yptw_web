let isPaused = false;
let intervalId;
let remainingTime;  // time in seconds

function startWork() {
    const amount = document.getElementById('amount').value;
    const hours = document.getElementById('hours').value;
    const minutes = document.getElementById('minutes').value;
    if (!amount || (!hours && !minutes)) {
        alert("Please enter an amount and at least one time value (hours or minutes).");
        return;
    }
    remainingTime = (parseInt(hours) * 3600 || 0) + (parseInt(minutes) * 60 || 0);  // convert to seconds
    document.getElementById('pauseBtn').style.display = 'block';  // Show the pause button
    document.getElementById('pauseBtn').disabled = false;  // Enable the pause button
    document.getElementById('formWrapper').style.display = 'none';
    document.getElementById('message').innerText = `You paid $${amount} to work ${hours ? hours + ' hours ' : ''}${minutes ? minutes + ' minutes' : ''}! Don’t waste it.`;
    updateTimer(remainingTime);
    intervalId = setInterval(() => {
        if (!isPaused && remainingTime > 0) {
            remainingTime -= 1;
            updateTimer(remainingTime);
        } else if (remainingTime <= 0) {
            clearInterval(intervalId);  // stop the timer when time is up
            document.getElementById('formWrapper').style.display = 'block';
            document.getElementById('pauseBtn').style.display = 'none';  // Hide the pause button
        }
    }, 1000);  // update every second
}




function pauseWork() {
    isPaused = !isPaused;
    const pauseBtn = document.getElementById('pauseBtn');
    if (isPaused) {
        pauseBtn.innerText = 'Resume';
    } else {
        pauseBtn.innerText = 'Pause';
    }
}

function updateTimer(remainingTime) {
    const hours = Math.floor(remainingTime / 3600);
    const minutes = Math.floor((remainingTime % 3600) / 60);
    const seconds = remainingTime % 60;
    document.getElementById('timer').innerText = 
        `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
    if (remainingTime <= 0) {
        document.getElementById('formWrapper').style.display = 'block';
        document.getElementById('pauseBtn').style.display = 'none';  // Hide the pause button
    }
}